package com.sda.datasourcelayer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Contract entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CONTRACT", catalog = "sda")
public class ContractEntity implements java.io.Serializable {

	// Fields

	private Integer id;
	private ClientEntity client;
	private String title;
	private String content;
	private String state;
	private Integer merit;

	// Constructors

	/** default constructor */
	public ContractEntity() {
	}

	/** minimal constructor */
	public ContractEntity(ClientEntity client, String title, String content) {
		this.client = client;
		this.title = title;
		this.content = content;
	}

	/** full constructor */
	public ContractEntity(ClientEntity client, String title, String content,
			String state, Integer merit) {
		this.client = client;
		this.title = title;
		this.content = content;
		this.state = state;
		this.merit = merit;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENT_ID", nullable = false)
	public ClientEntity getClient() {
		return this.client;
	}

	public void setClient(ClientEntity client) {
		this.client = client;
	}

	@Column(name = "TITLE", nullable = false, length = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "CONTENT", nullable = false)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "STATE", length = 500)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "MERIT")
	public Integer getMerit() {
		return this.merit;
	}

	public void setMerit(Integer merit) {
		this.merit = merit;
	}

}