package com.sda.datasourcelayer;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Client entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CLIENT", catalog = "sda")
public class ClientEntity implements java.io.Serializable {

	// Fields

	private Integer id;
	private String clienttitle;
	private Boolean isvip;
	private Integer version;
	private Set<ContractEntity> contracts = new HashSet<ContractEntity>(0);

	// Constructors

	/** default constructor */
	public ClientEntity() {
	}

	/** minimal constructor */
	public ClientEntity(String clienttitle, Boolean isvip, Integer version) {
		this.clienttitle = clienttitle;
		this.isvip = isvip;
		this.version = version;
	}

	/** full constructor */
	public ClientEntity(String clienttitle, Boolean isvip, Integer version,
			Set<ContractEntity> contracts) {
		this.clienttitle = clienttitle;
		this.isvip = isvip;
		this.version = version;
		this.contracts = contracts;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CLIENTTITLE", nullable = false, length = 500)
	public String getClienttitle() {
		return this.clienttitle;
	}

	public void setClienttitle(String clienttitle) {
		this.clienttitle = clienttitle;
	}

	@Column(name = "ISVIP", nullable = false)
	public Boolean getIsvip() {
		return this.isvip;
	}

	public void setIsvip(Boolean isvip) {
		this.isvip = isvip;
	}

	@Version
	@Column(name = "VERSION", nullable = false)
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "client")
	public Set<ContractEntity> getContracts() {
		return this.contracts;
	}

	public void setContracts(Set<ContractEntity> contracts) {
		this.contracts = contracts;
	}

}