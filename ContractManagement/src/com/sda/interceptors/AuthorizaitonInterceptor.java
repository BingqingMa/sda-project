package com.sda.interceptors;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class AuthorizaitonInterceptor extends MethodFilterInterceptor {

	@Override
	protected String doIntercept(ActionInvocation actionInvocation)
			throws Exception {
		String username = ServletActionContext.getRequest().getParameter(
				"username");
		String role = ServletActionContext.getRequest().getParameter("role");
		if (username != null && role != null) {
			return actionInvocation.invoke();
		} else {
			return Action.INPUT;
		}
	}
}
