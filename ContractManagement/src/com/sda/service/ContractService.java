package com.sda.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.sda.datasourcelayer.ClientEntity;
import com.sda.datasourcelayer.ContractEntity;
import com.sda.domain.Client;
import com.sda.domain.Contract;

/**
 * Service facade for the contract service, expose the and organize the
 * interfaces provided by domain model. This is the service wraps the contract
 * operations for different users.
 */

public class ContractService extends ActionSupport {

	private Contract contract;
	private Client client;
	private List<ContractEntity> contracts;
	private List<ClientEntity> clients;
	private String clientID;
	private String error;

	private String username;
	private String role;

	private String title;
	private String content;

	public String listClients() {
		setClients(client.getAllClients());
		return SUCCESS;
	}

	public String draftContract() {
		clearFieldErrors();

		ContractEntity newContractEntity = new ContractEntity();
		newContractEntity.setClient(client.findClientEntityById(Integer
				.parseInt(clientID)));
		newContractEntity.setTitle(title);
		newContractEntity.setContent(content);
		if (contract.draft(newContractEntity)) {
			return SUCCESS;
		}
		return INPUT;
	}

	public String getAllContracts() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		username = request.getParameter("username");
		role = request.getParameter("role");
		setContracts(contract.getAllContracts());
		return SUCCESS;
	}

	public String delete() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		if (contract.delete(Integer.parseInt(request.getParameter("id")))) {
			return SUCCESS;
		}
		return INPUT;
	}

	public String edit() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		contract.findContractEntityById(id);
		return SUCCESS;
	}

	public String sign() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		String role = request.getParameter("role");
		String state = contract.getCurrentState(id);
		if (state == null) {
			state = " sign by " + role + "; ";
		} else if (!state.contains("sign by " + role)
				&& !state.contains("declined by " + role)) {
			state += " sign by " + role + "; ";
		}
		contract.changeState(id, state);
		return SUCCESS;
	}

	public String decline() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		String role = request.getParameter("role");
		String state = contract.getCurrentState(id);
		if (state == null) {
			state = " declined by " + role + "; ";
		} else if (!state.contains("declined by " + role)
				&& !state.contains("sign by " + role)) {
			state += " declined by " + role + "; ";
		}
		contract.changeState(id, state);
		return SUCCESS;
	}

	public String approve() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		String role = request.getParameter("role");
		String state = "";
		if (contract.getCurrentState(id).contains("sign by Auditor")
				&& contract.getCurrentState(id).contains("sign by Solicitor")) {
			state = " approved by " + role + "; ";
		} else {
			error = "Invalid Action!";
			return INPUT;
		}
		contract.changeState(id, state);
		return SUCCESS;
	}

	public String view() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		setTitle(contract.findContractEntityById(id).getTitle());
		setContent(contract.findContractEntityById(id).getContent());
		return SUCCESS;
	}

	public String weigh() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		int id = Integer.parseInt(request.getParameter("id"));
		int weight = Integer.parseInt(request.getParameter("weight"));
		if (contract.findContractEntityById(id).getMerit() != null) {
			contract.weigh(id, weight
					+ contract.findContractEntityById(id).getMerit());
		} else {
			System.out.println(weight);
			contract.weigh(id, weight);
		}
		return SUCCESS;
	}

	// Properties Accessors.
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public List<ContractEntity> getContracts() {
		return contracts;
	}

	public void setContracts(List<ContractEntity> contracts) {
		this.contracts = contracts;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<ClientEntity> getClients() {
		return clients;
	}

	public void setClients(List<ClientEntity> clients) {
		this.clients = clients;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
