package com.sda.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.opensymphony.xwork2.ActionSupport;
import com.sda.domain.Authentication;

/**
 * Service facade for the authentication service, expose the and organize the
 * interfaces provided by domain model. This is the service wraps the
 * authentication.
 */

public class AuthenticationService extends ActionSupport {

	private Authentication auth;
	private String username;
	private String password;
	private String role;

	// private HttpServletRequest request;

	@Override
	public String execute() throws Exception {
		clearFieldErrors();
		// HttpServletRequest req = (HttpServletRequest) request;
		// HttpSession session = req.getSession();
		if (auth.login(username, password)) {
			System.out.print("AAAAAAAA");
			role = auth.getUserDAO().findByUsername(username).get(0).getRole();
			return SUCCESS;
		} else {
			return INPUT;
		}
	}

	// @Override
	// public void validate() {
	// if (null == this.getUsername() || "".equals(this.getUsername().trim())) {
	// this.addFieldError("invalid", "Username cannot be blank.");
	// }
	//
	// if (null == this.getPassword() || "".equals(this.getPassword().trim())) {
	// this.addFieldError("invalid", "Password cannot be blank");
	// }
	// }

	// Properties Accessors.
	public Authentication getAuth() {
		return auth;
	}

	public void setAuth(Authentication auth) {
		this.auth = auth;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	// public HttpServletRequest getRequest() {
	// return request;
	// }
	//
	// public void setRequest(HttpServletRequest request) {
	// this.request = request;
	// }

}
