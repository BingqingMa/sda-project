package com.sda.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.sda.datasourcelayer.ClientEntity;
import com.sda.domain.Client;

public class ClientService extends ActionSupport {

	private Client client;
	private ClientEntity clientEntity;
	private List<ClientEntity> clients;

	private String username;
	private String role;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public ClientEntity getClientEntity() {
		return clientEntity;
	}

	public void setClientEntity(ClientEntity clientEntity) {
		this.clientEntity = clientEntity;
	}

	public List<ClientEntity> getClients() {
		return clients;
	}

	public void setClients(List<ClientEntity> clients) {
		this.clients = clients;
	}

	public String addClient() {
		clearFieldErrors();
		if (client.add(clientEntity)) {
			return SUCCESS;
		}
		return INPUT;
	}

	public String getAllClients() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		username = request.getParameter("username");
		role = request.getParameter("role");
		setClients(client.getAllClients());
		return SUCCESS;
	}

	public String delelteClient() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		if (client.delete(Integer.parseInt(request.getParameter("id")))) {
			return SUCCESS;
		}
		return INPUT;
	}

	public String findClientById() {
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		username = request.getParameter("username");
		role = request.getParameter("role");
		int id = Integer.parseInt(request.getParameter("id"));
		ClientEntity clientEntity = client.findClientEntityById(id);
		setClientEntity(clientEntity);
		return SUCCESS;
	}

	public String editClient() {
		client.changeClientTitle(clientEntity.getId(),
				clientEntity.getClienttitle());
		client.changeClientVIP(clientEntity.getId(), clientEntity.getIsvip());
		return SUCCESS;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
