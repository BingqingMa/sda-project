package com.sda.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.opensymphony.xwork2.ActionSupport;
import com.sda.datasourcelayer.UserEntity;
import com.sda.domain.Register;

/**
 * Service facade for the registration service, expose the and organize the
 * interfaces provided by domain model. This is the service wraps the
 * registration.
 */

public class RegisterService extends ActionSupport {
	private Register register;
	private UserEntity user;
	private String message;

	@Override
	public String execute() throws Exception {
		clearFieldErrors();
		if (register.register(user)) {
			message = "Registration is done.";
			return SUCCESS;
		} else {
			addFieldError("invalid", "Username already exists.");
			return INPUT;
		}
	}

	@Override
	public void validate() {
		if (null == this.user.getUsername()
				|| "".equals(this.user.getUsername().trim())) {
			this.addFieldError("invalid", "Username cannot be blank.");
		}

		if (null == this.user.getPassword()
				|| "".equals(this.user.getPassword().trim())) {
			this.addFieldError("invalid", "Password cannot be blank");
		}

		if (null == this.user.getEmail()
				|| "".equals(this.user.getEmail().trim())) {
			this.addFieldError("invalid", "Email cannot be blank");
		} else {
			Pattern pattern = Pattern.compile(".+@.+\\..+");
			Matcher matcher = pattern.matcher(this.user.getEmail());
			if (!matcher.matches()) {
				this.addFieldError("invalid",
						"Please enter a valid email address.");
			}
		}

		if (null == this.user.getRole()
				|| "".equals(this.user.getRole().trim())) {
			this.addFieldError("invalid", "Role cannot be blank");
		}

	}

	// Properties Accessors.
	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	public UserEntity getUserEntity() {
		return user;
	}

	public void setUserEntity(UserEntity user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
