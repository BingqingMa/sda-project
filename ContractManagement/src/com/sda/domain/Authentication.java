package com.sda.domain;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.UserEntity;
import com.sda.mapper.UserDAO;

/**
 * Domain Model Authentication. @author BingqingMa Domain model containing
 * authentication domain logic.
 */

public class Authentication {
	private static final Logger log = LoggerFactory
			.getLogger(Authentication.class);
	// Database accessor for user data.
	private UserDAO userDAO;

	// Business logic for user login.
	@Transactional
	public boolean login(String username, String password) {
		try {
			List<UserEntity> list = userDAO.findByUsername(username);
			System.out.print(list.size());
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getPassword().equals(password)) {
					return true;
				}
			}
		} catch (RuntimeException re) {
			log.debug("Authtication Login Failed");
			return false;
		}
		return false;
	}

	// Property accessors
	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
