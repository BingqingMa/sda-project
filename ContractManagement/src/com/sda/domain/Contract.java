package com.sda.domain;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.ContractEntity;
import com.sda.mapper.ContractDAO;

public class Contract {

	// Database accessor for contract data.
	private ContractDAO contractDAO;

	// Draft a contract.
	@Transactional
	public boolean draft(ContractEntity contract) {
		try {
			contractDAO.save(contract);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Change the contract state.
	@Transactional
	public boolean changeState(int id, String state) {
		try {
			findContractEntityById(id).setState(state);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Delete a contract.
	@Transactional
	public boolean delete(int contractId) {
		try {
			contractDAO.delete(contractDAO.findById(contractId));
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Change the merits of the contract.
	@Transactional
	public boolean weigh(int id, int weight) {
		try {
			findContractEntityById(id).setMerit(weight);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Get the current state of contract.
	@Transactional
	public String getCurrentState(int id) {
		return findContractEntityById(id).getState();
	}

	// Get all the contracts information.
	@Transactional
	public List<ContractEntity> getAllContracts() {
		return contractDAO.findAll();
	}

	// Get the contract information by contract Id.
	@Transactional
	public ContractEntity findContractEntityById(int id) {
		return contractDAO.findById(id);
	}

	// Property accessors
	public ContractDAO getContractDAO() {
		return contractDAO;
	}

	public void setContractDAO(ContractDAO contractDAO) {
		this.contractDAO = contractDAO;
	}
}
