package com.sda.domain;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.ClientEntity;
import com.sda.mapper.ClientDAO;

public class Client {

	// Database accessor for client data.
	private ClientDAO clientDAO;

	// Add a client
	@Transactional
	public boolean add(ClientEntity client) {
		try {
			clientDAO.save(client);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Delete a client
	@Transactional
	public boolean delete(int clientID) {
		try {
			clientDAO.delete(clientDAO.findById(clientID));
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Get all clients information
	@Transactional
	public List<ClientEntity> getAllClients() {
		return clientDAO.findAll();
	}

	// Get the client information by client Id.
	@Transactional
	public ClientEntity findClientEntityById(int clientID) {
		return clientDAO.findById(clientID);
	}

	// Change client's title
	@Transactional
	public boolean changeClientTitle(int clientID, String title) {
		try {
			clientDAO.findById(clientID).setClienttitle(title);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	// Change client's VIP status
	@Transactional
	public boolean changeClientVIP(int clientID, boolean isVIP) {
		try {
			clientDAO.findById(clientID).setIsvip(isVIP);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	public ClientDAO getClientDAO() {
		return clientDAO;
	}

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}
}
