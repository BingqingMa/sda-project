package com.sda.domain;

import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.UserEntity;
import com.sda.mapper.UserDAO;

public class Register {
	private UserDAO userDAO;

	@Transactional
	public boolean register(UserEntity user) {
		try {
			if (userDAO.findByUsername(user.getUsername()).size() > 0)
				return false;
			else
				userDAO.save(user);
		} catch (RuntimeException re) {
			return false;
		}
		return true;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
}
