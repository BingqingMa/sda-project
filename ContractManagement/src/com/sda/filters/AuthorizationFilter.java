package com.sda.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

public class AuthorizationFilter extends HttpServlet implements Filter {

	@Override
	public void doFilter(ServletRequest sRequest, ServletResponse sResponse,
			FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) sRequest;
		HttpServletResponse response = (HttpServletResponse) sResponse;
		String url = request.getServletPath();
		String contextPath = request.getContextPath();
		if (url.equals(""))
			url += "/";

		if (url.startsWith("/register")) {
		} else if (url.startsWith("/contractManagement")) {
			if (null == request.getParameter("role")) {
				response.sendRedirect(contextPath + "/index.jsp");
				return;
			}
		} else if (url.startsWith("/addClient")) {
			if (null == request.getParameter("role")
					|| !request.getParameter("role").equals("Salesman")) {
				response.sendRedirect(contextPath + "/index.jsp");
				return;
			}
		} else if (url.startsWith("/draftContract")) {

			if (null == request.getParameter("role")
					|| !request.getParameter("role").equals("Salesman")) {
				response.sendRedirect(contextPath + "/index.jsp");
				return;
			}
		} else {
			if ((url.startsWith("/") && !url.startsWith("/index"))) {
				response.sendRedirect(contextPath + "/index.jsp");
				return;
			}
		}
		filterChain.doFilter(sRequest, sResponse);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
