package com.sda.mapper;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.ContractEntity;

/**
 * A data access object (DAO) providing persistence and search support for
 * Contract entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.sda.datasourcelayer.ContractEntity
 * @author MyEclipse Persistence Tools
 */
@Transactional
public class ContractDAO {
	private static final Logger log = LoggerFactory
			.getLogger(ContractDAO.class);
	// property constants
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String STATE = "state";
	public static final String MERIT = "merit";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(ContractEntity transientInstance) {
		log.debug("saving ContractEntity instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ContractEntity persistentInstance) {
		log.debug("deleting ContractEntity instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ContractEntity findById(java.lang.Integer id) {
		log.debug("getting ContractEntity instance with id: " + id);
		try {
			ContractEntity instance = (ContractEntity) getCurrentSession().get(
					"com.sda.datasourcelayer.ContractEntity", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<ContractEntity> findByExample(ContractEntity instance) {
		log.debug("finding ContractEntity instance by example");
		try {
			List<ContractEntity> results = (List<ContractEntity>) getCurrentSession()
					.createCriteria("com.sda.datasourcelayer.ContractEntity")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ContractEntity instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from ContractEntity as model where model."
					+ propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<ContractEntity> findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List<ContractEntity> findByContent(Object content) {
		return findByProperty(CONTENT, content);
	}

	public List<ContractEntity> findByState(Object state) {
		return findByProperty(STATE, state);
	}

	public List<ContractEntity> findByMerit(Object merit) {
		return findByProperty(MERIT, merit);
	}

	public List findAll() {
		log.debug("finding all ContractEntity instances");
		try {
			String queryString = "from ContractEntity";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ContractEntity merge(ContractEntity detachedInstance) {
		log.debug("merging ContractEntity instance");
		try {
			ContractEntity result = (ContractEntity) getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ContractEntity instance) {
		log.debug("attaching dirty ContractEntity instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ContractEntity instance) {
		log.debug("attaching clean ContractEntity instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static ContractDAO getFromApplicationContext(ApplicationContext ctx) {
		return (ContractDAO) ctx.getBean("ContractDAO");
	}
}