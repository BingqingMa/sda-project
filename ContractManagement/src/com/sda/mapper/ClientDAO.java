package com.sda.mapper;

import java.util.List;
import java.util.Set;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.sda.datasourcelayer.ClientEntity;

/**
 * A data access object (DAO) providing persistence and search support for
 * Client entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.sda.datasourcelayer.ClientEntity
 * @author MyEclipse Persistence Tools
 */
@Transactional
public class ClientDAO {
	private static final Logger log = LoggerFactory.getLogger(ClientDAO.class);
	// property constants
	public static final String CLIENTTITLE = "clienttitle";
	public static final String ISVIP = "isvip";
	public static final String VERSION = "version";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
	}

	public void save(ClientEntity transientInstance) {
		log.debug("saving ClientEntity instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(ClientEntity persistentInstance) {
		log.debug("deleting ClientEntity instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ClientEntity findById(java.lang.Integer id) {
		log.debug("getting ClientEntity instance with id: " + id);
		try {
			ClientEntity instance = (ClientEntity) getCurrentSession().get(
					"com.sda.datasourcelayer.ClientEntity", id);
			sessionFactory.getCurrentSession().lock(instance, LockMode.READ);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<ClientEntity> findByExample(ClientEntity instance) {
		log.debug("finding ClientEntity instance by example");
		try {
			List<ClientEntity> results = (List<ClientEntity>) getCurrentSession()
					.createCriteria("com.sda.datasourcelayer.ClientEntity")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding ClientEntity instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from ClientEntity as model where model."
					+ propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<ClientEntity> findByClienttitle(Object clienttitle) {
		return findByProperty(CLIENTTITLE, clienttitle);
	}

	public List<ClientEntity> findByIsvip(Object isvip) {
		return findByProperty(ISVIP, isvip);
	}

	public List<ClientEntity> findByVersion(Object version) {
		return findByProperty(VERSION, version);
	}

	public List findAll() {
		log.debug("finding all ClientEntity instances");
		try {
			String queryString = "from ClientEntity";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public ClientEntity merge(ClientEntity detachedInstance) {
		log.debug("merging ClientEntity instance");
		try {
			ClientEntity result = (ClientEntity) getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(ClientEntity instance) {
		log.debug("attaching dirty ClientEntity instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ClientEntity instance) {
		log.debug("attaching clean ClientEntity instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static ClientDAO getFromApplicationContext(ApplicationContext ctx) {
		return (ClientDAO) ctx.getBean("ClientDAO");
	}
}