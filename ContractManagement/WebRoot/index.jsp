<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html lang='en'>
<head>
<meta charset="UTF-8" />
<title>HTML Document Structure</title>
<link rel="stylesheet" type="text/css" href="css/style-login.css" />
</head>
<body>

	<div id="wrapper">

		<s:form name="login-form" cssClass="login-form" action="LoginAction"
			method="post">

			<div class="header">
				<h1>Login Form</h1>
			</div>

			<div class="content">
				<s:textfield name="username" cssClass="input username"
					placeholder="Username" />
				<div class="user-icon"></div>
				<s:password name="password" cssClass="input password"
					placeholder="Password" />
				<div class="pass-icon"></div>
			</div>

			<div class="footer">
				<input type="submit" name="submit" value="Login" class="button" />
				<a href="register.jsp" class="register">Register</a>
			</div>

		</s:form>

	</div>
	<div class="gradient"></div>
</body>
</html>
