<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Contract Project</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>
<body>
	<!-- Header -->
	<div id="header">
		<div class="shell">
			<!-- Logo + Top Nav -->
			<div id="top">
				<h1>
					<strong>Contract Project</strong>
				</h1>
				<div id="top-navigation">
					Welcome <strong><s:property value="username" /> (<s:property
							value="role" />)</strong> | <a href="index.jsp">Log out</a>
				</div>
			</div>
			<!-- End Logo + Top Nav -->

			<!-- Main Nav -->
			<div id="navigation">
				<ul>

					<li><s:url id="contractManagementURL" action="ListAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{contractManagementURL}">
							<span>Contract Management</span>
						</s:a></li>
					<li><s:url id="clientManagementURL"
							action="clientManagementAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{clientManagementURL}">
							<span>Client Management</span>
						</s:a></li>
				</ul>
			</div>
			<!-- End Main Nav -->
		</div>
	</div>

</body>
</html>