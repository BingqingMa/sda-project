<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Contract Project</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>
<body>
	<!-- Header -->
	<div id="header">
		<div class="shell">
			<!-- Logo + Top Nav -->
			<div id="top">
				<h1>
					<strong>Contract Project</strong>
				</h1>
				<div id="top-navigation">
					Welcome <strong><s:property value="username" /> (<s:property
							value="role" />)</strong> | <a href="index.jsp">Log out</a>
				</div>
			</div>
			<!-- End Logo + Top Nav -->

			<!-- Main Nav -->
			<div id="navigation">
				<ul>

					<li><s:url id="contractManagementURL" action="ListAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{contractManagementURL}">
							<span>Contract Management</span>
						</s:a></li>
					<li><s:url id="clientManagementURL"
							action="clientManagementAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{clientManagementURL}">
							<span>Client Management</span>
						</s:a></li>
				</ul>
			</div>
			<!-- End Main Nav -->
		</div>
	</div>
	<!-- Container -->
	<div id="container">
		<div class="shell">

			<!-- Small Nav -->
			<div class="small-nav">
				<s:url id="contractManagementURL" action="ListAction">
					<s:param name="username" value="username"></s:param>
					<s:param name="role" value="role"></s:param>
				</s:url>
				<s:a href="%{contractManagementURL}">Contract Management</s:a>
				<span>&gt;</span> Current Contracts

			</div>
			<!-- End Small Nav -->

			<!-- Main -->
			<div id="main">
				<div class="cl">&nbsp;</div>

				<!-- Content -->
				<div id="content">

					<!-- Box -->
					<div class="box">
						<!-- Box Head -->
						<div class="box-head">
							<h2 class="left">Current Contracts</h2>
						</div>
						<!-- End Box Head -->

						<!-- Table -->
						<div class="table">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th width="20%">Title</th>
									<th width="30%">State</th>
									<th width="10%">Merit</th>
									<th width="40%" class="ac">Content Control</th>
								</tr>
								<s:iterator value="contracts" id="c">
									<tr>
										<td><h3>
												<s:property value="#c.title"></s:property>
											</h3></td>
										<td><s:property value="#c.state"></s:property></td>
										<td><s:property value="#c.merit"></s:property></td>
										<s:url id="deleteURL" action="deleteContractAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="editURL" action="editContractAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="approveURL" action="approveAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="declineURL" action="declineAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="signURL" action="signAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="viewURL" action="viewAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>

										<td class="ac"><s:if test="%{role=='Salesman'}">
												<s:a href="%{deleteURL}" class="ico del">Delete</s:a>
											</s:if> <s:if
												test="%{role=='Senior Manager'|| role=='Sales Manager'}">
												<s:a href="%{approveURL}">Approve</s:a>
											</s:if> <s:a href="%{signURL}">Sign</s:a> <s:a href="%{declineURL}">Decline</s:a>
											<s:a href="%{viewURL}">View</s:a> <s:if
												test="%{role=='Auditor'||role=='Solicitor'}">
												<select name="weight_s">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>
												<s:url id="weighURL" action="weighAction">
													<s:param name="id" value="%{#c.id}"></s:param>
													<s:param name="weight" value="2"></s:param>
													<s:param name="username" value="username"></s:param>
													<s:param name="role" value="role"></s:param>
												</s:url>
												<s:a href="%{weighURL}">Weigh</s:a>
											</s:if></td>
									</tr>
								</s:iterator>
							</table>

						</div>

						<!-- Table -->

					</div>
					<!-- End Box -->

				</div>
				<!-- End Content -->

				<!-- Sidebar -->
				<div id="sidebar">

					<!-- Box -->
					<div class="box">

						<!-- Box Head -->
						<div class="box-head">
							<h2>Management</h2>
						</div>
						<!-- End Box Head-->

						<s:if test="%{role=='Salesman'}">
							<div class="box-content">
								<a href="<s:url action="listClientsAction"/>" class="add-button"><span>Add
										new contract</span></a>
								<div class="cl">&nbsp;</div>
							</div>
						</s:if>
					</div>
					<!-- End Box -->
				</div>
				<!-- End Sidebar -->

				<div class="cl">&nbsp;</div>
			</div>
			<!-- Main -->
		</div>
	</div>
	<!-- End Container -->

	<!-- Footer -->
	<div id="footer">
		<div class="shell">
			<span class="left">&copy; 2014 - Bingqing Ma and Yingyang Liu</span>
		</div>
	</div>
	<!-- End Footer -->

</body>
</html>