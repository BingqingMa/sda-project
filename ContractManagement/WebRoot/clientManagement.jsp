<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<title>Contract Project</title>
</head>
<body>
	<!-- Header -->
	<div id="header">
		<div class="shell">
			<!-- Logo + Top Nav -->
			<div id="top">
				<h1>
					<strong>Contract Project</strong>
				</h1>
				<div id="top-navigation">
					Welcome <strong><s:property value="username" /> (<s:property
							value="role" />)</strong> | <a href="index.jsp">Log out</a>
				</div>
			</div>
			<!-- End Logo + Top Nav -->

			<!-- Main Nav -->
			<div id="navigation">
				<ul>

					<li><s:url id="contractManagementURL" action="ListAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{contractManagementURL}">
							<span>Contract Management</span>
						</s:a></li>
					<li><s:url id="clientManagementURL"
							action="clientManagementAction">
							<s:param name="username" value="username"></s:param>
							<s:param name="role" value="role"></s:param>
						</s:url> <s:a href="%{clientManagementURL}">
							<span>Client Management</span>
						</s:a></li>
				</ul>
			</div>
			<!-- End Main Nav -->
		</div>
	</div>
	<!-- Container -->
	<div id="container">
		<div class="shell">

			<!-- Small Nav -->
			<div class="small-nav">
				<s:url id="clientManagementURL" action="clientManagementAction">
					<s:param name="username" value="username"></s:param>
					<s:param name="role" value="role"></s:param>
				</s:url>
				<s:a href="%{clientManagementURL}">Client Management</s:a>
				<span>&gt;</span> Current Clients
			</div>
			<!-- End Small Nav -->

			<!-- Main -->
			<div id="main">
				<div class="cl">&nbsp;</div>

				<!-- Content -->
				<div id="content">

					<!-- Box -->
					<div class="box">
						<!-- Box Head -->
						<div class="box-head">
							<h2 class="left">Current Clients</h2>
						</div>
						<!-- End Box Head -->

						<!-- Table -->
						<div class="table">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th width="20%">Id</th>
									<th width="30%">Title</th>
									<th width="10%">VIP</th>
									<th width="40%" class="ac">Content Control</th>
								</tr>
								<s:iterator value="clients" id="c">
									<tr>
										<td><h3>
												<s:property value="#c.id"></s:property>
											</h3></td>
										<td><s:property value="#c.clienttitle"></s:property></td>
										<td><s:property value="#c.isvip"></s:property></td>
										<s:url id="deleteURL" action="deleteClientAction">
											<s:param name="id" value="%{#c.id}"></s:param>
											<s:param name="username" value="username"></s:param>
											<s:param name="role" value="role"></s:param>
										</s:url>
										<s:url id="editURL" action="findClientAction">
											<s:param name="id" value="%{#c.id}"></s:param>
										</s:url>
										<td class="ac"><s:if test="%{role=='Salesman'}">
												<s:a href="%{deleteURL}">Delete</s:a>
												<s:a href="%{editURL}">Edit</s:a>
											</s:if></td>
									</tr>
								</s:iterator>
							</table>

						</div>

						<!-- Table -->

					</div>
					<!-- End Box -->

				</div>
				<!-- End Content -->

				<!-- Sidebar -->
				<div id="sidebar">

					<!-- Box -->
					<div class="box">

						<!-- Box Head -->
						<div class="box-head">
							<h2>Management</h2>
						</div>
						<!-- End Box Head-->
						<s:if test="%{role=='Salesman'}">
							<div class="box-content">
								<s:url id="addClientURL" value="addClient.jsp">
									<s:param name="username" value="username"></s:param>
									<s:param name="role" value="role"></s:param>
								</s:url>
								<s:a href="%{addClientURL}" cssClass="add-button">
									<span>Add new Client</span>
								</s:a>
								<div class="cl">&nbsp;</div>

							</div>
						</s:if>
					</div>
					<!-- End Box -->
				</div>
				<!-- End Sidebar -->

				<div class="cl">&nbsp;</div>
			</div>
			<!-- Main -->
		</div>
	</div>
	<!-- End Container -->

	<!-- Footer -->
	<div id="footer">
		<div class="shell">
			<span class="left">&copy; 2014 - Bingqing Ma and Yingyang Liu</span>
		</div>
	</div>
	<!-- End Footer -->
</body>
</html>