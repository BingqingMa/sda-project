<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String username = request.getParameter("username");
	String role = request.getParameter("role");
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Add Client</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>

<body>
	<!-- Message Error -->
	<div class="msg msg-error">
		<p>
			<strong><s:fielderror name="invalid" /></strong>
		</p>
		<a href="addClient.jsp" class="close">close</a>
	</div>

	<!-- Box -->
	<div class="box">
		<!-- Box Head -->
		<div class="box-head">
			<h2>Add New Client</h2>
		</div>
		<!-- End Box Head -->

		<s:form action="addClientAction" method="POST">

			<!-- Form -->
			<div class="form">
				<p>
					<span class="req">max 100 symbols</span> <label>Client
						Title <span>(Required Field)</span>
					</label>
					<s:textfield name="clientEntity.clienttitle" cssClass="field size1" />
				</p>

				<p>
					<label>VIP <span>(Required Field)</span></label>
					<s:select name="clientEntity.isvip" list="{'false','true'}"
						cssClass="field size1"></s:select>
				</p>
				<p>
					<s:hidden name="username" value="Tom"></s:hidden>
				</p>
				<p>
					<s:hidden name="role" value="Salesman"></s:hidden>
				</p>

			</div>
			<!-- End Form -->

			<!-- Form Buttons -->
			<div class="buttons">
				<input type="submit" class="button" value="submit" />
			</div>
			<!-- End Form Buttons -->
		</s:form>
	</div>
	<!-- End Box -->
	<!-- Footer -->
	<div id="footer">
		<div class="shell">
			<span class="left">&copy; 2014 - Bingqing Ma and Yingyang Liu</span>
		</div>
	</div>
	<!-- End Footer -->
</body>
</html>