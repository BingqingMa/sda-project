<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Draft Contract</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>

<body>

	<!-- Box -->
	<div class="box">
		<!-- Box Head -->
		<div class="box-head">
			<h2>View Contract</h2>
		</div>
		<!-- End Box Head -->
		<s:form action="" method="POST">

			<div class="form">
				<p>
					<label>Contract Title </label>
					<s:textfield name="title" value="%{title}" readonly="true"
						cssClass="field size1" />
				</p>

				<p>
					<label>Content</label>
					<s:textarea name="content" value="%{content}" readonly="true"
						cssClass="field size1" rows="30" cols="50" />
				</p>
			</div>
		</s:form>
	</div>
	<!-- End Box -->
	<!-- Footer -->
	<div id="footer">
		<div class="shell">
			<span class="left">&copy; 2014 - Bingqing Ma and Yingyang Liu</span>
		</div>
	</div>
	<!-- End Footer -->
</body>
</html>